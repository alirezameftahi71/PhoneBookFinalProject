package test.java;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collection;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.NoContentException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import main.java.model.Role;
import main.java.model.person.Person;
import main.java.model.person.dao.PersonDAO;

public class PersonDaoTest {

	PersonDAO personDAO = null;
	Person person = null;

	@Before
	public void setup() throws SQLIntegrityConstraintViolationException {
		personDAO = new PersonDAO();
		person = new Person();
		person.setUsername("Alireza");
		person.setPassword("Meftahi");
		person.setRole(Role.ADMIN);
		personDAO.add(person);
	}

	@Test
	public void add() throws NoContentException {
		Assert.assertEquals(person, personDAO.getById(person.getId()));
	}

	@Test
	public void update() throws NoContentException {
		Person changedPerson = new Person();
		changedPerson.setId(person.getId());
		changedPerson.setUsername("Mohammad");
		changedPerson.setPassword(person.getPassword());
		changedPerson.setRole(person.getRole());
		personDAO.update(changedPerson);
		Assert.assertNotEquals(person, personDAO.getById(person.getId()));
	}

	@Test
	public void getById() throws NoContentException {
		Person person2 = personDAO.getById(person.getId());
		Assert.assertEquals(person, person2);
	}

	@Test(expected = NotFoundException.class)
	public void delete() throws NoContentException {
		personDAO.delete(person);
		personDAO.getById(person.getId());
	}

	@Test
	public void getAll() throws SQLIntegrityConstraintViolationException {
		Collection<Person> persons = null;
		persons = personDAO.getAll();
		int sizeOne = persons.size();
		personDAO.add(person);
		persons = personDAO.getAll();
		int sizeTwo = persons.size();
		Assert.assertEquals(sizeTwo, (sizeOne + 1));
	}

}
