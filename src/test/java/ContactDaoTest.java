package test.java;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collection;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.NoContentException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import main.java.model.contact.Contact;
import main.java.model.contact.dao.ContactDAO;

public class ContactDaoTest {

	ContactDAO contactDAO = null;
	Contact contact = null;

	@Before
	public void setup() throws SQLIntegrityConstraintViolationException, UnrecognizedPropertyException {
		contactDAO = new ContactDAO();
		contact = new Contact();
		contact.setFirstName("Alireza");
		contact.setLastName("Meftahi");
		contact.setHomeTel("0263460");
		contact.setMobile("09393744769");
		contact.setEmail("alirezameftahi_71@yahoo.com");
		contactDAO.add(contact);
	}

	@Test
	public void add() throws NoContentException {
		Assert.assertEquals(contact, contactDAO.getById(contact.getId()));
	}

	@Test
	public void update() throws NoContentException {
		Contact changedPerson = new Contact();
		changedPerson.setId(contact.getId());
		changedPerson.setFirstName("Mohammad");
		changedPerson.setLastName(contact.getLastName());
		changedPerson.setHomeTel(contact.getHomeTel());
		changedPerson.setMobile(contact.getMobile());
		changedPerson.setEmail(contact.getEmail());
		contactDAO.update(changedPerson);
		Assert.assertNotEquals(contact, contactDAO.getById(contact.getId()));
	}

	@Test
	public void getById() throws NoContentException {
		Assert.assertEquals(contact, contactDAO.getById(contact.getId()));
	}

	@Test(expected = NotFoundException.class)
	public void delete() throws NoContentException {
		contactDAO.delete(contact);
		contactDAO.getById(contact.getId());
	}

	@Test
	public void getAll() throws SQLIntegrityConstraintViolationException, UnrecognizedPropertyException {
		Collection<Contact> contacts = null;
		contacts = contactDAO.getAll();
		int sizeOne = contacts.size();
		contactDAO.add(contact);
		contacts = contactDAO.getAll();
		int sizeTwo = contacts.size();
		Assert.assertEquals(sizeTwo, (sizeOne + 1));
	}

}
