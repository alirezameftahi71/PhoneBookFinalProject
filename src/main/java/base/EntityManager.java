package main.java.base;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collection;
import javax.ws.rs.core.NoContentException;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

public interface EntityManager<E> {

	void add(E e) throws SQLIntegrityConstraintViolationException;
	
	E getById(Integer id) throws NoContentException;

	void update(E e);

	void delete(E e);

	Collection<E> list();

}
