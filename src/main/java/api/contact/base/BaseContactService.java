package main.java.api.contact.base;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collection;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NoContentException;
import javax.ws.rs.core.Response;
import main.java.base.AbstractEntityService;
import main.java.model.contact.Contact;
import main.java.model.contact.base.BaseContactManager;

/*
 * Full Path: 
 * http://localhost:8080/JeePhoneBook/api/contact/items
 */

@Path("/contact/items")
public class BaseContactService extends AbstractEntityService<Contact> {

	@Override
	@POST
	public Response add(Contact contact) {
		try {
			BaseContactManager.getInstance().add(contact);
		} catch (IllegalArgumentException e) {
			return Response.status(400).entity(e.getMessage()).build();
		} catch (SQLIntegrityConstraintViolationException e) {
			return Response.status(409).entity(e.getMessage()).build();
		}
		return Response.ok().entity("Contact Added Successfully.").build();
	}

	@Override
	@GET
	@Path("/{id}")
	public Response getById(@PathParam("id") Integer id) {
		Contact contact = null;
		try {
			contact = BaseContactManager.getInstance().getById(id);
		} catch (NoContentException e) {
			return Response.serverError().entity(e.getMessage()).build();
		} catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity("Entity not found for Contact ID: " + id).build();
		}
		return Response.ok(contact, MediaType.APPLICATION_JSON).build();
	}

	@Override
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Integer id) {
		Contact contact = null;
		try {
			contact = BaseContactManager.getInstance().getById(id);
			BaseContactManager.getInstance().delete(contact);
		} catch (NoContentException e) {
			return Response.serverError().entity(e.getMessage()).build();
		} catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
		}
		return Response.ok().entity("Contact Deleted Successfully.").build();
	}

	@Override
	@PUT
	public Response update(Contact contact) {
		try {
			BaseContactManager.getInstance().getById(contact.getId());
			BaseContactManager.getInstance().update(contact);
		} catch (NoContentException e) {
			return Response.serverError().entity(e.getMessage()).build();
		} catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
		}
		return Response.ok().entity("Contact Updated Successfully.").build();
	}

	@Override
	@GET
	public Response getAll() {
		Collection<Contact> contacts = null;
		try {
			contacts = BaseContactManager.getInstance().list();
		} catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
		}
		return Response.ok(contacts, MediaType.APPLICATION_JSON).build();
	}

}
