package main.java.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/test")
public class TestConnection {

	@GET
	public boolean isConnected() {
		return true;
	}
}
