package main.java.api.person.base;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collection;
import javax.validation.ValidationException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NoContentException;
import javax.ws.rs.core.Response;
import main.java.base.AbstractEntityService;
import main.java.model.person.Person;
import main.java.model.person.base.BasePersonManager;
import main.java.model.person.logic.PersonManager;

/*
 * Full Path: 
 * http://localhost:8080/JeePhoneBook/api/person/items
 */

@Path("/person/items")
public class BasePersonService extends AbstractEntityService<Person> {

	@Override
	@POST
	public Response add(Person person) {
		try {
			BasePersonManager.getInstance().add(person);
		} catch (IllegalArgumentException e) {
			return Response.status(400).entity(e.getMessage()).build();
		} catch (SQLIntegrityConstraintViolationException e) {
			return Response.status(409).entity(e.getMessage()).build();
		}
		return Response.ok().entity("Person Added Successfully.").build();
	}

	@Override
	@GET
	@Path("/{id}")
	public Response getById(@PathParam("id") Integer id) {
		Person person = null;
		try {
			person = BasePersonManager.getInstance().getById(id);
		} catch (NoContentException e) {
			return Response.serverError().entity(e.getMessage()).build();
		} catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity("Entity not found for Person ID: " + id).build();
		}
		return Response.ok(person, MediaType.APPLICATION_JSON).build();
	}

	@Override
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Integer id) {
		Person person = null;
		try {
			person = BasePersonManager.getInstance().getById(id);
			BasePersonManager.getInstance().delete(person);
		} catch (NoContentException e) {
			return Response.serverError().entity(e.getMessage()).build();
		} catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
		}
		return Response.ok().entity("Person Deleted Successfully.").build();
	}

	@Override
	@PUT
	public Response update(Person person) {
		try {
			BasePersonManager.getInstance().getById(person.getId());
			BasePersonManager.getInstance().update(person);
		} catch (NoContentException e) {
			return Response.serverError().entity(e.getMessage()).build();
		} catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
		}
		return Response.ok().entity("Person Updated Successfully.").build();
	}

	@Override
	@GET
	public Response getAll() {
		Collection<Person> persons = null;
		try {
			persons = BasePersonManager.getInstance().list();
		} catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
		}
		return Response.ok(persons, MediaType.APPLICATION_JSON).build();
	}

	@POST
	@Path("/login")
	public Response login(Person person) {
		try {
			PersonManager personManager = new PersonManager();
			personManager.login(person);
		} catch (NoContentException e) {
			return Response.serverError().entity(e.getMessage()).build();
		} catch (ValidationException e) {
			return Response.status(401).entity(e.getMessage()).build();
		}
		return Response.ok().entity("Welcome dear " + person.getUsername()).build();
	}
	
}
