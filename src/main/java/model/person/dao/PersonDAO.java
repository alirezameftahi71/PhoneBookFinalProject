package main.java.model.person.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import main.java.model.person.Person;
import main.java.model.person.base.BasePersonDAO;

public class PersonDAO extends BasePersonDAO {
	public Person validate(Person person) throws IndexOutOfBoundsException {
		if (person == null)
			throw new IllegalArgumentException("No Content was Recieved.");
		Person p = null;
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			p = (Person) session.createQuery("from Person where username=:place")
					.setParameter("place", person.getUsername()).list().get(0);
			tx.commit();
			return p;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
