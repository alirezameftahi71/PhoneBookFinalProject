package main.java.model.person.base;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collection;

import javax.persistence.PersistenceException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.NoContentException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import main.java.base.AbstractEntityDAO;
import main.java.model.person.Person;

public class BasePersonDAO extends AbstractEntityDAO<Person> {

	protected BasePersonDAO() {

	}

	@Override
	public void add(Person person) throws SQLIntegrityConstraintViolationException {
		if (person == null)
			throw new IllegalArgumentException("No Content was Recieved.");
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.save(person);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} catch (PersistenceException e) {
			throw new SQLIntegrityConstraintViolationException(
					"Duplicate entry '" + person.getId() + "' for key 'PRIMARY'");
		} finally {
			session.close();
		}

	}

	@Override
	public void update(Person person) {
		if (person == null)
			throw new IllegalArgumentException("No Content was Recieved.");
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.update(person);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

	}

	@Override
	public Person getById(Integer id) throws NoContentException {
		if (id == null)
			throw new NoContentException("Person ID cannot be blank.");
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			Person person = (Person) session.get(Person.class, id);
			tx.commit();
			if (person == null)
				throw new NotFoundException("Entity not found for Person ID: " + id);
			return person;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public void delete(Person person) {
		if (person == null)
			throw new IllegalArgumentException("No Content was Recieved.");
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.delete(person);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	@Override
	public Collection<Person> getAll() {
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			Collection<Person> result = session.createQuery("from Person").list();
			tx.commit();
			if (result.size() <= 0)
				throw new NotFoundException("No Entity found in DB.");
			return result;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

}
