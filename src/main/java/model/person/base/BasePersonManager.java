package main.java.model.person.base;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collection;

import javax.ws.rs.core.NoContentException;

import main.java.base.AbstractEntityManager;
import main.java.core.MD5;
import main.java.model.person.Person;

public class BasePersonManager extends AbstractEntityManager<Person> {

	public static BasePersonManager getInstance() {
		return new BasePersonManager();
	}

	public BasePersonDAO getDAO() {
		return new BasePersonDAO();
	}

	@Override
	public void add(Person person) throws SQLIntegrityConstraintViolationException {
		person.setPassword(MD5.getMD5(person.getPassword()));
		this.getDAO().add(person);
	}

	@Override
	public void update(Person person) {
		person.setPassword(MD5.getMD5(person.getPassword()));
		this.getDAO().update(person);
	}

	@Override
	public void delete(Person person) {
		this.getDAO().delete(person);
	}

	@Override
	public Collection<Person> list() {
		return this.getDAO().getAll();
	}

	@Override
	public Person getById(Integer id) throws NoContentException {
		return this.getDAO().getById(id);
	}

}
