package main.java.model.person.logic;

import javax.validation.ValidationException;
import javax.ws.rs.core.NoContentException;
import main.java.model.person.Person;
import main.java.model.person.base.BasePersonManager;
import main.java.model.person.dao.PersonDAO;

public class PersonManager extends BasePersonManager {
	public boolean login(Person person) throws NoContentException {
		boolean validPerson = false;
		try {
			PersonDAO personDAO = new PersonDAO();
			Person p = personDAO.validate(person);
			if (!(p.getPassword().equals(person.getPassword())))
				throw new ValidationException("User or pass is wrong");
			else
				validPerson = true;
		} catch (IndexOutOfBoundsException e) {
			throw new ValidationException("User or pass is wrong");
		}
		return validPerson;
	}
}
