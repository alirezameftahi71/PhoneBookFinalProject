package main.java.model.contact.base;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collection;

import javax.persistence.PersistenceException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.NoContentException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import main.java.base.AbstractEntityDAO;
import main.java.model.contact.Contact;

/**
 * Log4j logger
 */
public class BaseContactDAO extends AbstractEntityDAO<Contact> {

	protected BaseContactDAO() {

	}

	@Override
	public void add(Contact contact) throws SQLIntegrityConstraintViolationException {
		if (contact == null)
			throw new IllegalArgumentException("No Content was Recieved.");
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.save(contact);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} catch (PersistenceException e) {
			throw new SQLIntegrityConstraintViolationException(
					"Duplicate entry '" + contact.getId() + "' for key 'PRIMARY'");
		} finally {
			session.close();
		}
	}

	@Override
	public void update(Contact contact) {
		if (contact == null)
			throw new IllegalArgumentException("No Content was Recieved.");
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.update(contact);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	@Override
	public Contact getById(Integer id) throws NoContentException {
		if (id == null)
			throw new NoContentException("Contact ID cannot be blank.");
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			Contact contact = (Contact) session.get(Contact.class, id);
			tx.commit();
			if (contact == null)
				throw new NotFoundException("Entity not found for Contact ID: " + id);
			return contact;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public void delete(Contact contact) {
		if (contact == null)
			throw new IllegalArgumentException("No Content was Recieved.");
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.delete(contact);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	@Override
	public Collection<Contact> getAll() {
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			Collection<Contact> result = session.createQuery("from Contact").list();
			tx.commit();
			if (result.size() <= 0)
				throw new NotFoundException("No Entity found in DB.");
			return result;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

}
