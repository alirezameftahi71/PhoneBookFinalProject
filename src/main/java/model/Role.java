package main.java.model;

import java.io.Serializable;

public enum Role implements Serializable {
	ADMIN, VIP, USER, GUEST;

	public String getStatus() {
		return this.name();
	}
}
